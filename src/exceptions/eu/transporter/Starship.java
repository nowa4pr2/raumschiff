package exceptions.eu.transporter;
/**
 * Diese Klasse kann eine Person von �from� aufs Raumschiff hochbeamen. 
 * Der Vorgang soll dazu den Transporter im �dringend� Modus verwenden.
 * 
 *
 */
public class Starship {
	
	private String name;
	private Transporter transporter;
	
	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Transporter getTransporter() {
		return transporter;
	}


	public void setTransporter(Transporter transporter) {
		this.transporter = transporter;
	}


	public Starship(String name, Transporter transporter) {
		this.name = name;
		this.transporter = transporter;
	}
	
	/**
	 * 
	 * @param person
	 * @param from
	 * @throws TransporterMalfunctionException
	 */
	public void beamUp(String person, String from) throws TransporterMalfunctionException {
		try {
				this.name = person;
				transporter.bean(person, from, "Raumschiff", true);
			
		} catch(TransporterMalfunctionException e) {
			System.out.println("Person:[" + person + "] - konnte nicht from:[" + from  + "]  to:[" + transporter.getTo() + "] gebeamt werden.");
		} finally {
			transporter.shutdow();
		}
	}
	

}
