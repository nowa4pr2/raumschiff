package exceptions.eu.transporter;

/**
 * Diese Klasse startet die Application Episode42. Ein Objekt Starship namens
 * “Enterprise” wird erzeugt und dieses Objekt kann dann Captain Kirk von Riga IV an Board
 * der Enterprise beamen. 
 *
 */
public class Episode42 {

	public static void main(String[] args) {

		Transporter transporter = null;
		try {

			transporter = new Transporter();

			Starship starship = new Starship("Captain Kirk", transporter);

			int i = 0;
			while (i <= 10) {
				starship.beamUp("Captain Kirk", "Riga IV");
				i++;
			}

		} catch (TransporterMalfunctionException e) {
			System.out.println("exception");
		}
	}

}
