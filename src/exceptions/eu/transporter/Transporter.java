package exceptions.eu.transporter;

/**
 * Diese Klasse kann eine Person von "from" nach "to" beamen und hat falls Modus
 * �dringend� (urgent==true) eine 70%ige Ausfallswahrscheinlichkeit. Die
 * m�gliche Fehlfunktion soll druch eine TransporterMalfunctionExeption Nach
 * jedem Beam-Vorgang muss unabh�ngig ob dieser erfolgreich war oder nicht
 * jedenfalls die shutdown Methode des Tansporters aufgerufen werden.
 *
 */
public class Transporter {

	private String to;

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	/**
	 * Diese Methode transportiert Person from to
	 * 
	 * @param person
	 * @param from
	 * @param to
	 * @param urgent
	 * @throws TransporterMalfunctionException
	 *             - wenn wahrscheinlich gr��er als 0.7
	 *             TransporterMalfunctionException wird geworfen
	 */
	public void bean(String person, String from, String to, boolean urgent) throws TransporterMalfunctionException {
		this.to = to;

		double wahrscheinlichkeit = Math.random();

		if (wahrscheinlichkeit > 0.7) {
			if (urgent == true) {
				throw new TransporterMalfunctionException();
			}
		}

		System.out.println("Person:[" + person + "] - wird urgent from:[ " + from + "] to:[ " + to + "] gebeamt.");
	}

	/**
	 * Diese Methode dient zur Information vom Shoutdown und die Ausgabe erfolgt
	 * in der Konsole
	 */
	public void shutdow() {
		System.out.println("Shutdown");
	}

}
